package com.example.restservice;

public class User {


	private   String name;
	private String lastName;
    private  String email;
	private  String password;
	static long id_acum=0;
	private long id;
	

	public User(String myName, String myEmail, String myPassword) {
		this.name = myName;
        this.email =myEmail;
		this.password= myPassword;
		id = id_acum;
		id_acum++;

	}

	public String getName() {
		return  name;
    }
    
    public String getPassword() {
		return password;
	}
	public long getId() {
		return id;
	}

	public String getEmail() {
		return email;
	}


	//setters
	public void setPassword(String pass) {
		this.password=pass;
	}
	public void setName(String myName) {
		this.name=myName;
	}

	public void setEmail(String myEmail) {
		this.email=myEmail;
	}
    
}