package com.example.restservice;

import java.util.concurrent.atomic.AtomicLong;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.DeleteMapping;

import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;

 
import java.util.ArrayList;
import java.util.List;
 
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;



@RestController
public class UserController {



    static List<User> userList=new ArrayList<User>();
    static {
         
        userList.add(new User("jorge","jorge@hootmail.com","123"));
        userList.add(new User("juan","juan@hootmail.com","123"));
         
    }


	@GetMapping("/users")
    public List<User> getUsers() {
        return userList;
    }

    @GetMapping("/users/{id}")
    public ResponseEntity<Object>  getUser(@PathVariable long id) {
        for(int i = 0; i < userList.size(); i++)
        {
            if (userList.get(i).getId()==id){
                return new ResponseEntity<Object>(userList.get(i), HttpStatus.OK);
          
            }
             
        }
        return new ResponseEntity<>("Unable to find user: " + 
        id , 
                HttpStatus.NOT_FOUND);
    
    }

   
    @DeleteMapping("/users/{id}")
    public ResponseEntity<Object>  deleteUser(@PathVariable long id) {
        for(int i = 0; i < userList.size(); i++)
        {
            if (userList.get(i).getId()==id){
                userList.remove(i);
                return new ResponseEntity<>("User " + id + " deleted.", HttpStatus.OK);
            }
             
        }
        return new ResponseEntity<>("Unable to delete  user with id " + 
        id + " not exist.", 
                HttpStatus.NOT_FOUND);
    
    }

    @PostMapping(path="/users",consumes="application/json")
    public ResponseEntity<Object> createUser(@RequestBody User user) {
        
        User newUser=  new User(user.getName(), user.getEmail(), user.getPassword());
        for(int i = 0; i < userList.size(); i++)
        {
            if (userList.get(i).getEmail().equals(newUser.getEmail()))

            return new ResponseEntity<>("Unable to create. A User with email " + user.getEmail() + " already exist.", 
                HttpStatus.CONFLICT);
               
        }
        
        userList.add(newUser);
        HttpHeaders headers = new HttpHeaders();

        return new ResponseEntity<>("User created  with email: " + user.getEmail() +" and name: " + user.getName() ,  HttpStatus.CREATED);
        
        
    }

    // ------------------- Update a User ------------------------------------------------
 
    @PutMapping( "/users/{id}")
    public ResponseEntity<?> updateUser(@PathVariable("id") long id, @RequestBody User user) {
       
        User currentUser= null;
        int i=0;
        for( i =0; i < userList.size(); i++)
        {
            if (userList.get(i).getId()==id)
              
                currentUser= userList.get(i);
        }
      
 
        if (currentUser == null) {

            return new ResponseEntity<>("Unable to upate. User with id " + id + " not found.", 
                     HttpStatus.NOT_FOUND);
  
           
        }
 
        currentUser.setName(user.getName());
        currentUser.setEmail(user.getEmail());
        currentUser.setPassword(user.getPassword());

       
      //  userList.set(i , currentUser);

        return new ResponseEntity<User>(currentUser, HttpStatus.OK);
    }
}
